package com.seqato.seqatotest;

import androidx.lifecycle.ViewModel;

/**
 * @author Samuel Robert <samuel.robert@seqato.com>
 * @created on 07 Sep 2018 at 4:45 PM
 * <p>
 * TODO: Use this class for storing view related data
 * TODO: You may use LiveData to handle the interaction between views and repository
 * TODO: Using Dagger for dependency Injection will be a plus
 */
public class AuthViewModel extends ViewModel {
}

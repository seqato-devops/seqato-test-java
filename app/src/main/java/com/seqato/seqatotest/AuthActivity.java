package com.seqato.seqatotest;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

/**
 * @author Samuel Robert <samuel.robert@seqato.com>
 * @created on 06 Sep 2018 at 1:00 PM
 * @see AuthViewModel
 * @see AuthRepository
 * <p>
 * TODO: Writing test cases will be a plus
 */
public class AuthActivity extends AppCompatActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth);
		
		// TODO: Use vm to save UI data into the view model
		AuthViewModel vm = ViewModelProviders.of(this).get(AuthViewModel.class);
	}
}
